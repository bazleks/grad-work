package ru.etu.sim.gui;

import ru.etu.sim.genetic.GeneticParams;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.*;

public class GAParamsDialog extends JDialog {

    private GeneticParams geneticParams;

    // region swing elements

    private JPanel rootPanel;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JSlider slider1;
    private JSlider slider2;
    private JTextField pitchMutationField;
    private JTextField targetMutationField;
    private JTextField targetRangeField;
    private JTextField pitchRangeField;
    private JPanel controlButtons;
    private JComboBox<GeneticParams.GAStrategy> comboBox1;
    private JLabel populationLabel;
    private JLabel generationLabel;

    // endregion

    public GAParamsDialog(GeneticParams geneticParams) {
        this.geneticParams = geneticParams;
        setContentPane(rootPanel);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });
        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });
        comboBox1.setModel(new DefaultComboBoxModel<>(GeneticParams.GAStrategy.values()));
        slider1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int populationCount = slider1.getValue();
                populationLabel.setText(Integer.toString(populationCount));
            }
        });
        slider2.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int iterationCount = slider2.getValue();
                generationLabel.setText(Integer.toString(iterationCount));
            }
        });
        fillGeneticParams(geneticParams);


// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        rootPanel.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        slider1.addComponentListener(new ComponentAdapter() {
        });
    }

    private void onOK() {
// add your code here
        try {
            geneticParams.setIterationsCount(slider1.getValue());
            geneticParams.setPopulationCount(slider2.getValue());
            geneticParams.setTargetDeltaDev(Double.parseDouble(targetRangeField.getText()));
            geneticParams.setTargetMutAff(Double.parseDouble(targetMutationField.getText()));
            geneticParams.setPitchDeltaDev(Double.parseDouble(pitchRangeField.getText()));
            geneticParams.setPitchMutAff(Double.parseDouble(pitchMutationField.getText()));
            geneticParams.setStrategy((GeneticParams.GAStrategy) comboBox1.getSelectedItem());
            System.out.println("Dispose");
            dispose();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPanel, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    private void fillGeneticParams(GeneticParams geneticParams) {
        slider1.setValue(geneticParams.getIterationsCount());
        slider2.setValue(geneticParams.getPopulationCount());
        targetRangeField.setText(String.valueOf(geneticParams.getTargetDeltaDev()));
        targetMutationField.setText(String.valueOf(geneticParams.getTargetMutAff()));
        pitchRangeField.setText(String.valueOf(geneticParams.getPitchDeltaDev()));
        pitchMutationField.setText(String.valueOf(geneticParams.getTargetMutAff()));
        comboBox1.setSelectedItem(geneticParams.getStrategy());
    }

    public static void setDefaultGeneticParams(GeneticParams geneticParams) {
        geneticParams.setPopulationCount(50);
        geneticParams.setIterationsCount(50);
        geneticParams.setTargetDeltaDev(1);
        geneticParams.setTargetMutAff(1);
        geneticParams.setPitchDeltaDev(1);
        geneticParams.setPitchMutAff(1);
        geneticParams.setStrategy(GeneticParams.GAStrategy.ONLY_TARGET);
    }


}
