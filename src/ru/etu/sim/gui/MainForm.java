package ru.etu.sim.gui;

import ru.etu.sim.genetic.FullSolution;
import ru.etu.sim.genetic.GeneticParams;
import ru.etu.sim.launchers.*;
import ru.etu.sim.launchers.genetics.FullGeneticLauncher;
import ru.etu.sim.launchers.genetics.OnlyPitchGeneticLauncher;
import ru.etu.sim.launchers.genetics.OnlyTargetGeneticLauncher;
import ru.etu.sim.launchers.genetics.SequencedTargetFirstLauncher;
import ru.spb.nicetu.launchsimulation.simcore.math.function.PieceLinearFunction;
import ru.spb.nicetu.launchsimulation.simcore.math.specificmatrixalgebra.GeographicCoordinatePoint;
import ru.spb.nicetu.launchsimulation.simcore.simobject.Launch;
import ru.spb.nicetu.launchsimulation.simcore.simobject.rocket.state.CalculationStrategy;
import ru.spb.nicetu.launchsimulation.simtestdata.TestDataInits;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author bazleks
 */
public class MainForm {

    private GeneticParams geneticParams;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        JFrame frame = new JFrame("MainForm");
        frame.setContentPane(new MainForm().rootPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    // region UI components
    private JPanel rootPanel;
    private JTable pitchTable;
    private JPanel paramsPanel;
    private JPanel strategyPanel;
    private JRadioButton reductiveRadioButton;
    private JRadioButton onlyDragRadioButton;
    private JRadioButton onlyCoriolisRadioButton;
    private JRadioButton realRadioButton;
    private JPanel targetPitchContainer;
    private JPanel pitchPanel;
    private JButton addButton;
    private JButton deleteButton;
    private JPanel targetPanel;
    private JPanel resultPanel;
    private JScrollPane resultTextScroll;
    private JTextArea resultTextArea;
    private JComboBox<TestDataInits.Variants> defLaunchComboBox;
    private JCheckBox customLaunchCheckBox;
    private JPanel launchCorrectionPanel;
    private JCheckBox useGeneticCorrectionCheckBox;
    private JButton showGeneticParametersButton;
    private JScrollPane tableScroll;
    private JTextField startedLatitudeField;
    private JTextField startedLongitudeField;
    private JTextField targetLatitudeField;
    private JTextField targetLongitudeField;
    private JButton poehaliButton;
    private JProgressBar progressBar1;
    // endregion

    public MainForm() {
        initUIComponents();
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final Double[] pitchEntry = new Double[2];
                AddPitchDialog addPitchDialog = new AddPitchDialog(pitchEntry);
                addPitchDialog.setTitle("Add pitch entry");
                addPitchDialog.pack();
                addPitchDialog.setVisible(true);
                if (pitchEntry[0] != null && pitchEntry[1] != null) {
                    final DefaultTableModel model = (DefaultTableModel) pitchTable.getModel();
                    model.addRow(pitchEntry);
                }
            }
        });
    }

    private void initUIComponents() {

        // region radio button group
        ButtonGroup radioButtonsGroup = new ButtonGroup();
        radioButtonsGroup.add(reductiveRadioButton);
        radioButtonsGroup.add(onlyDragRadioButton);
        radioButtonsGroup.add(onlyCoriolisRadioButton);
        radioButtonsGroup.add(realRadioButton);
        // endregion

        // region check boxes
        useGeneticCorrectionCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showGeneticParametersButton.setEnabled(useGeneticCorrectionCheckBox.isSelected());
            }
        });
        customLaunchCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final boolean customLaunchSelected = customLaunchCheckBox.isSelected();
                defLaunchComboBox.setEnabled(!customLaunchSelected);
                startedLatitudeField.setEnabled(customLaunchSelected);
                startedLongitudeField.setEnabled(customLaunchSelected);
                targetLatitudeField.setEnabled(customLaunchSelected);
                targetLongitudeField.setEnabled(customLaunchSelected);
                addButton.setEnabled(customLaunchSelected);
                deleteButton.setEnabled(customLaunchSelected);
                pitchTable.setCellSelectionEnabled(false);
            }
        });
        // endregion

        // region default launch params combo box
        ComboBoxModel<TestDataInits.Variants> modelVariants = new DefaultComboBoxModel<>(TestDataInits.Variants.values());
        defLaunchComboBox.setModel(modelVariants);
        defLaunchComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TestDataInits.variant = (TestDataInits.Variants) defLaunchComboBox.getSelectedItem();

                GeographicCoordinatePoint startPoint = TestDataInits.getStartPoint();
                GeographicCoordinatePoint targetPoint = TestDataInits.getTargetPoint();
                startedLatitudeField.setText(Double.toString(Math.toDegrees(startPoint.getLatitude())));
                startedLongitudeField.setText(Double.toString(Math.toDegrees(startPoint.getLongitude())));
                targetLatitudeField.setText(Double.toString(Math.toDegrees(targetPoint.getLatitude())));
                targetLongitudeField.setText(Double.toString(Math.toDegrees(targetPoint.getLongitude())));

                ((DefaultTableModel) pitchTable.getModel()).setRowCount(0);
                PieceLinearFunction pitchProgram = TestDataInits.getPichProgram();
                for (Double t : pitchProgram.getFunctionArguments()) {
                    Object[] row = new Object[]{t, Math.toDegrees(pitchProgram.getFunctionValue(t))};
                    System.out.println(String.format("t:%s, value:%s", row[0], row[1]));
                    ((DefaultTableModel) pitchTable.getModel()).addRow(row);
                }
            }
        });
        // endregion

        // region poehali button
        poehaliButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startModelling();
//                try {
//                    LaunchParams launchParams = fillLaunchParams();
//                    GeneticParams geneticParams = fillGAParams();
//
//                    final RocketLauncher launcher;
//                    if (useGeneticCorrectionCheckBox.isSelected()) {
//                        launcher = new GeneticCorrectionLauncher(launchParams, geneticParams);
//
//
//                    } else {
//                        launcher = new DefaultLauncher(launchParams);
//                        launcher.launch();
//                    }
//
//                    String launchInfo = constructLaunchInfo(launcher, geneticParams);
//                    printResult(launchInfo);
//                    Thread thread = new Thread(launcher);
//                    thread.start();
//                } catch (IllegalStateException ex) {
//                    JOptionPane.showMessageDialog(rootPanel, ex.getMessage());
//                }


            }
        });
        // endregion

        pitchTable.setModel(new DefaultTableModel(new String[]{"Time", "Pitch"}, 0));
        defLaunchComboBox.setSelectedIndex(0);

        showGeneticParametersButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (geneticParams == null) {
                    geneticParams = new GeneticParams();
                    GAParamsDialog.setDefaultGeneticParams(geneticParams);
                }
                GAParamsDialog dialog = new GAParamsDialog(geneticParams);
                dialog.setTitle("Genetic algorithm parameters");
                dialog.pack();
                dialog.setVisible(true);
            }
        });
    }

    private void startModelling() {
        printResult("***********************************");
        printResult("Start modelling...");
        System.out.println("geneticParams = " + geneticParams);
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    setGUIEnabled(false);
                    poehaliButton.setEnabled(false);
                    LaunchParams launchParams = fillLaunchParams();
                    AbstractLauncher launcher = defineLauncher();
                    launcher.setLaunchParams(launchParams);
                    launcher.launch();
                    setGUIEnabled(true);
                } catch (Exception e) {
                    setGUIEnabled(true);
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(rootPanel, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    printResult("Error! " + e.getMessage());
                }
            }
        });
        thread.setName("Poehali thread");
        thread.start();
    }

    private void setGUIEnabled(boolean enabled) {
        poehaliButton.setEnabled(enabled);
        reductiveRadioButton.setEnabled(enabled);
        onlyDragRadioButton.setEnabled(enabled);
        onlyCoriolisRadioButton.setEnabled(enabled);
        realRadioButton.setEnabled(enabled);
        defLaunchComboBox.setEnabled(enabled);
        customLaunchCheckBox.setEnabled(enabled);
        useGeneticCorrectionCheckBox.setEnabled(enabled);

    }

    private AbstractLauncher defineLauncher() {
        AbstractLauncher launcher;
        if (useGeneticCorrectionCheckBox.isSelected()) {
            if (geneticParams == null) {
                geneticParams = new GeneticParams();
                GAParamsDialog.setDefaultGeneticParams(geneticParams);
            }
            switch (geneticParams.getStrategy()) {
                case ONLY_TARGET:
                    launcher = new OnlyTargetGeneticLauncher(geneticParams,
                            new GeneticLauncher.GeneticLauncherListener<FullSolution.TargetSolution>() {
                                @Override
                                public void onNextSolutionStart(ModellingInstance modellingInstance) {
                                    Launch launch = modellingInstance.getModellingManager().getLaunch();
                                    fillTargetPoint(launch.getTargetPoint());
                                }

                                @Override
                                public void onNextSolutionDone(ModellingInstance modellingInstance, FullSolution.TargetSolution solution) {
                                    printResult(String.format(
                                                    "Current solution:{dlat,dlong}={%s,%s}, miss distance=%s",
                                                    solution.getDLat(), solution.getDLong(), modellingInstance.getMissingDistance())
                                    );
                                }

                                @Override
                                public void onFinish(ModellingInstance instance) {
                                    printResult("B-E-S-T   S-O-L-U-T-I-O-N");
                                    GeographicCoordinatePoint targetPoint = instance.getModellingManager().getLaunch().getTargetPoint();
                                    printResult(String.format("{lat,long)={%s,%s}", targetPoint.getLatitude(), targetPoint.getLongitude()));
                                    printResult(String.format("missing distance = %s", instance.getMissingDistance()));
                                }
                            });
                    break;
                case ONLY_PITCH:
                    launcher = new OnlyPitchGeneticLauncher(geneticParams, new GeneticLauncher.GeneticLauncherListener<FullSolution.PitchSolution>() {
                        @Override
                        public void onNextSolutionStart(ModellingInstance modellingInstance) {
                            fillPitchProgram(modellingInstance.getModellingManager().getLaunch().getPichControlProgram());
                            final PieceLinearFunction pitchControlProgram = modellingInstance.getModellingManager().getLaunch().getPichControlProgram();
                            for (Double t : pitchControlProgram.getFunctionArguments()) {
                                printResult(String.format("{%s,%s}", t, pitchControlProgram.getFunctionValue(t)));
                            }
                        }

                        @Override
                        public void onNextSolutionDone(ModellingInstance modellingInstance, FullSolution.PitchSolution solution) {
                            printResult("* * * * * * * * * * * * * * *");
                            printResult("Next solution: ");
                            for (FullSolution.PitchSolution.PitchDelta pitchDelta : solution.getPitchValues()) {
                                printResult("pitch delta: " + pitchDelta.getDPitch());
                            }
                            printResult(String.format("missing distance = %s", modellingInstance.getMissingDistance()));
                        }

                        @Override
                        public void onFinish(ModellingInstance instance) {
                            printResult("B-E-S-T   S-O-L-U-T-I-O-N");
                            final PieceLinearFunction pichControlProgram = instance.getModellingManager().getLaunch().getPichControlProgram();
                            for (Double t : pichControlProgram.getFunctionArguments()) {
                                printResult(String.format("{t,value}={%s, %s}", t, pichControlProgram.getFunctionValue(t)));
                            }
                            printResult(String.format("missing distance = %s", instance.getMissingDistance()));
                        }
                    });
                    break;
                case MIXED:
                    launcher = new FullGeneticLauncher(geneticParams, new GeneticLauncher.GeneticLauncherListener<FullSolution>() {
                        @Override
                        public void onNextSolutionStart(ModellingInstance modellingInstance) {
                            printResult("-> Next solution....");
                        }

                        @Override
                        public void onNextSolutionDone(ModellingInstance modellingInstance, FullSolution solution) {
                            printResult("Missing distance: " + modellingInstance.getMissingDistance());
                        }

                        @Override
                        public void onFinish(ModellingInstance instance) {
                            printResult("B-E-S-T   S-O-L-U-T-I-O-N");
                            GeographicCoordinatePoint targetPoint = instance.getModellingManager().getLaunch().getTargetPoint();
                            printResult("Target point: ");
                            printResult(String.format("\tlatitude = %s", targetPoint.getLatitude()));
                            printResult(String.format("\tlongitude = %s", targetPoint.getLongitude()));
                            printResult("Pitch control program: ");
                            final PieceLinearFunction pichControlProgram = instance.getModellingManager().getLaunch().getPichControlProgram();
                            for (Double t : pichControlProgram.getFunctionArguments()) {
                                printResult(String.format("\t{t,value}={%s, %s}", t, pichControlProgram.getFunctionValue(t)));
                            }
                            printResult(" ");
                            printResult(String.format("Missing distance = %s", instance.getMissingDistance()));
                        }
                    });
                    break;
                case SEQUENCED:
                    launcher = new SequencedTargetFirstLauncher(geneticParams, new SequencedTargetFirstLauncher.SequencedGeneticListener() {
                        @Override
                        public void onFinish(ModellingInstance instance) {
                            printResult("B-E-S-T   S-O-L-U-T-I-O-N");
                            GeographicCoordinatePoint targetPoint = instance.getModellingManager().getLaunch().getTargetPoint();
                            printResult("Target point: ");
                            printResult(String.format("\tlatitude = %s", targetPoint.getLatitude()));
                            printResult(String.format("\tlongitude = %s", targetPoint.getLongitude()));
                            printResult("Pitch control program: ");
                            final PieceLinearFunction pichControlProgram = instance.getModellingManager().getLaunch().getPichControlProgram();
                            for (Double t : pichControlProgram.getFunctionArguments()) {
                                printResult(String.format("\t{t,value}={%s, %s}", t, pichControlProgram.getFunctionValue(t)));
                            }
                            printResult(" ");
                            printResult(String.format("Missing distance = %s", instance.getMissingDistance()));
                        }
                    });
                    break;
                default:
                    throw new IllegalArgumentException(String.format("This strategy {%s} has not implemented yet!", geneticParams.getStrategy()));
            }
        } else {
            launcher = new DefaultLauncher(new AbstractLauncher.LauncherListener() {

                @Override
                public void onFinish(ModellingInstance instance) {
                    printResult("Modelling done! Miss distance: " + instance.getMissingDistance());
                }
            });
        }
        return launcher;
    }

    private LaunchParams fillLaunchParams() throws IllegalStateException {
        LaunchParams launchParams = new LaunchParams();
        launchParams.setCalculationStrategy(defineRadioGroupStrategy());
        if (customLaunchCheckBox.isSelected()) {
            try {
                launchParams.setStartedPoint(new GeographicCoordinatePoint(
                        Double.parseDouble(startedLatitudeField.getText()),
                        Double.parseDouble(startedLongitudeField.getText()),
                        0
                ));
            } catch (NumberFormatException e) {
                throw new IllegalStateException("Wrong started point: " + e.getMessage());
            }
            try {
                launchParams.setAimingPoint(new GeographicCoordinatePoint(
                        Double.parseDouble(targetLatitudeField.getText()),
                        Double.parseDouble(targetLongitudeField.getText()),
                        0
                ));
            } catch (NumberFormatException e) {
                throw new IllegalStateException("Wrong target point: " + e.getMessage());
            }
        } else {
            launchParams.setDataInitsVariant((TestDataInits.Variants) defLaunchComboBox.getSelectedItem());
            launchParams.setAimingPoint(TestDataInits.getTargetPoint());
        }
        PieceLinearFunction pitchProgram = new PieceLinearFunction();
        TableModel model = pitchTable.getModel();
        if (model.getRowCount() == 0) {
            throw new IllegalStateException("Wring pitch program: row count is equal to 0");
        }
        try {
            for (int i = 0; i < model.getRowCount(); i++) {
                Double t = (Double) model.getValueAt(i, 0);
                Double value = Math.toRadians((Double) model.getValueAt(i, 1));
                pitchProgram.setFunctionValue(value, t);
            }
            launchParams.setPitchProgram(pitchProgram);
        } catch (NumberFormatException e) {
            throw new IllegalStateException("Wrong pitch program: " + e.getMessage());
        }
        return launchParams;
    }

    private CalculationStrategy defineRadioGroupStrategy() throws IllegalStateException {
        if (reductiveRadioButton.isSelected()) return CalculationStrategy.REDUCTIVE;
        if (onlyDragRadioButton.isSelected()) return CalculationStrategy.ONLY_DRAG;
        if (onlyCoriolisRadioButton.isSelected()) return CalculationStrategy.ONLY_CORIOLIS;
        if (realRadioButton.isSelected()) return CalculationStrategy.REAL;
        throw new IllegalStateException("Calculation strategy is not set");
    }

//    private GeneticParams fillGAParams() throws IllegalStateException {
//        GeneticParams geneticParams = new GeneticParams();
//        geneticParams.setIterationsCount(50);
//        geneticParams.setPopulationCount(50);
//        geneticParams.setTargetDeltaDev(1d);
//        geneticParams.setStrategy(GeneticParams.GAStrategy.ONLY_TARGET);
//        return geneticParams;
//    }

//    private String constructLaunchInfo(RocketLauncher launcher, GeneticParams geneticParams) {
//        StringBuilder sb = new StringBuilder();
//        sb.append("* * * * * * * * * * * * * * * * * * * * * * * * * * * *\n");
//        if (launcher instanceof DefaultLauncher) {
//            sb.append("Modelling without genetic correction\n");
//        }
//        if (launcher instanceof GeneticCorrectionLauncher) {
//            sb.append("Modelling with genetic correction\n");
//        }
//        return sb.toString();
//    }

    private void printResult(String log) {
        resultTextArea.append("* ");
        resultTextArea.append(log);
        resultTextArea.append("\n");
    }

    private void fillTargetPoint(GeographicCoordinatePoint targetPoint) {

    }

    private void fillPitchProgram(PieceLinearFunction pitchProgram) {

    }
}