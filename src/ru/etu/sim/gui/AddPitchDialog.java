package ru.etu.sim.gui;

import javax.swing.*;
import java.awt.event.*;
import java.util.Map;

public class AddPitchDialog extends JDialog {
    private JPanel rootPanel;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField timeField;
    private JTextField pitchField;
    private JPanel buttonsPanel;
    private JPanel contentPanel;
    private JLabel timeLabel;
    private JLabel pitchLabel;

    private Double[] pitchEntry;

    public AddPitchDialog(Double[] pitchEntry) {
        setContentPane(rootPanel);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        this.pitchEntry = pitchEntry;

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        rootPanel.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        try {
            pitchEntry[0] = Double.valueOf(timeField.getText());
            pitchEntry[1] = Double.valueOf(pitchField.getText());
            dispose();
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(rootPanel, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

}
