package ru.etu.sim.launchers.genetics;

import ru.etu.sim.genetic.FullSolution;
import ru.etu.sim.genetic.GeneticParams;
import ru.etu.sim.launchers.LaunchParams;
import ru.etu.sim.launchers.ModellingInstance;
import ru.etu.sim.launchers.SequencedGenLauncher;
import ru.spb.nicetu.launchsimulation.simcore.math.specificmatrixalgebra.GeographicCoordinatePoint;

import java.util.Set;

/**
 * @author bazleks
 */
public class SequencedTargetFirstLauncher extends SequencedGenLauncher<OnlyTargetGeneticLauncher, OnlyPitchGeneticLauncher> {

    private OnlyPitchGeneticLauncher onlyPitchGeneticLauncher;
    private OnlyTargetGeneticLauncher onlyTargetGeneticLauncher;

    public interface SequencedGeneticListener extends LauncherListener {

    }

    public SequencedTargetFirstLauncher(GeneticParams geneticParams, SequencedGeneticListener launcherListener) {
        super(launcherListener);
        onlyPitchGeneticLauncher = new OnlyPitchGeneticLauncher(geneticParams, null);
        onlyTargetGeneticLauncher = new OnlyTargetGeneticLauncher(geneticParams, null);
    }

    @Override
    public void launch() {
        onlyPitchGeneticLauncher.setLaunchParams(launchParams);
        onlyPitchGeneticLauncher.launch();
        applyPitchProgram(launchParams, onlyPitchGeneticLauncher.getBestSolution());

        onlyTargetGeneticLauncher.setLaunchParams(launchParams);
        onlyTargetGeneticLauncher.launch();
        applyTargetPoint(launchParams, onlyTargetGeneticLauncher.getBestSolution());

        ModellingInstance modellingInstance = new ModellingInstance(launchParams);
        modellingInstance.calculate();
        getLauncherListener().onFinish(modellingInstance);
    }

    private void applyPitchProgram(LaunchParams launchParams, FullSolution.PitchSolution pitchSolution) {
        int i = 0;
        final Set<Double> functionArguments = launchParams.getPitchProgram().getFunctionArguments();
        for (Double key : functionArguments) {
            if (key == 0.1d) continue;
            final double value = launchParams.getPitchProgram().getFunctionValue(key) +
                    Math.toRadians(pitchSolution.getPitchValues().get(i++).getDPitch());
            launchParams.getPitchProgram().setFunctionValue(value, key);
        }
    }

    private void applyTargetPoint(LaunchParams launchParams, FullSolution.TargetSolution targetSolution) {
        final GeographicCoordinatePoint aimingPoint = launchParams.getAimingPoint();
        aimingPoint.setLatitude(aimingPoint.getLatitude() + targetSolution.getDLat());
        aimingPoint.setLongitude(aimingPoint.getLongitude() + targetSolution.getDLong());
    }
}
