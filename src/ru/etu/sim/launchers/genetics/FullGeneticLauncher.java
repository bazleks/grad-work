package ru.etu.sim.launchers.genetics;

import net.sourceforge.evoj.GenePool;
import net.sourceforge.evoj.PoolFactory;
import net.sourceforge.evoj.core.DefaultPoolFactory;
import ru.etu.sim.genetic.FullSolution;
import ru.etu.sim.genetic.GeneticParams;
import ru.etu.sim.launchers.GeneticLauncher;
import ru.etu.sim.launchers.LaunchParams;
import ru.etu.sim.launchers.ModellingInstance;
import ru.spb.nicetu.launchsimulation.simcore.math.function.PieceLinearFunction;
import ru.spb.nicetu.launchsimulation.simcore.math.specificmatrixalgebra.GeographicCoordinatePoint;

import java.util.HashMap;
import java.util.Map;

/**
 * @author bazleks
 */
public class FullGeneticLauncher extends GeneticLauncher<FullSolution> {
    private GeographicCoordinatePoint initialAimingPoint;
    private PieceLinearFunction initialPieceLinearFunction;

    public FullGeneticLauncher(GeneticParams geneticParams, GeneticLauncherListener<FullSolution> listener) {
        super(geneticParams, listener);
    }

    @Override
    public void setLaunchParams(LaunchParams launchParams) {
        super.setLaunchParams(launchParams);
        initialAimingPoint = new GeographicCoordinatePoint(launchParams.getAimingPoint());
        initialPieceLinearFunction = new PieceLinearFunction();
        for (Double t : launchParams.getPitchProgram().getFunctionArguments()) {
            initialPieceLinearFunction.setFunctionValue(launchParams.getPitchProgram().getFunctionValue(t), t);
        }
    }

    @Override
    public GenePool<FullSolution> createGenePool(int populationCount) {
        Map<String, String> context = new HashMap<>();
        context.put("pitch.length", String.valueOf(launchParams.getPitchProgram().getFunctionArguments().size()));
        context.put("latitude.min", String.valueOf(-geneticParams.getTargetDeltaDev()));
        context.put("latitude.max", String.valueOf(geneticParams.getTargetDeltaDev()));
        context.put("longitude.min", String.valueOf(-geneticParams.getTargetDeltaDev()));
        context.put("longitude.max", String.valueOf(geneticParams.getTargetDeltaDev()));
        context.put("pitch.angle.min", String.valueOf(-geneticParams.getPitchDeltaDev()));
        context.put("pitch.angle.max", String.valueOf(geneticParams.getPitchDeltaDev()));
        PoolFactory poolFactory = new DefaultPoolFactory();
        return poolFactory.createPool(populationCount, FullSolution.class, context);
    }

    @Override
    public void applySolution(FullSolution solution, ModellingInstance modellingInstance) {
        int i = 0;
        final FullSolution.TargetSolution targetSolution = solution.getTargetSolution();
        final FullSolution.PitchSolution pitchSolution = solution.getPitchSolution();

        modellingInstance.getModellingManager().getLaunch().getTargetPoint().setLatitude(
                initialAimingPoint.getLatitude() + targetSolution.getDLat());
        modellingInstance.getModellingManager().getLaunch().getTargetPoint().setLongitude(
                initialAimingPoint.getLongitude() + targetSolution.getDLong());

        final PieceLinearFunction pichControlProgram = modellingInstance.getModellingManager().getLaunch().getPichControlProgram();
        for (Double t : pichControlProgram.getFunctionArguments()) {
            if (t == 0.1d) continue;
            double value = initialPieceLinearFunction.getFunctionValue(t) + Math.toRadians(pitchSolution.getPitchValues().get(i++).getDPitch());
            pichControlProgram.setFunctionValue(value, t);
        }
    }
}
