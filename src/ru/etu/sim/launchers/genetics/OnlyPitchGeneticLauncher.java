package ru.etu.sim.launchers.genetics;

import net.sourceforge.evoj.GenePool;
import net.sourceforge.evoj.PoolFactory;
import net.sourceforge.evoj.core.DefaultPoolFactory;
import ru.etu.sim.genetic.FullSolution;
import ru.etu.sim.genetic.GeneticParams;
import ru.etu.sim.launchers.GeneticLauncher;
import ru.etu.sim.launchers.LaunchParams;
import ru.etu.sim.launchers.ModellingInstance;
import ru.spb.nicetu.launchsimulation.simcore.math.function.PieceLinearFunction;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author bazleks
 */
public class OnlyPitchGeneticLauncher extends GeneticLauncher<FullSolution.PitchSolution> {

    private PieceLinearFunction initialPitchFunction;

    public OnlyPitchGeneticLauncher(GeneticParams geneticParams, GeneticLauncherListener<FullSolution.PitchSolution> listener) {
        super(geneticParams, listener);
    }

    @Override
    public void setLaunchParams(LaunchParams launchParams) {
        super.setLaunchParams(launchParams);
        initialPitchFunction = new PieceLinearFunction();
        for (Double t : launchParams.getPitchProgram().getFunctionArguments()) {
            initialPitchFunction.setFunctionValue(launchParams.getPitchProgram().getFunctionValue(t), t);
        }
    }

    @Override
    public GenePool<FullSolution.PitchSolution> createGenePool(int populationCount) {
        Map<String, String> context = new HashMap<>();
        int size = launchParams.getPitchProgram().getFunctionArguments().size() - 1;
        context.put("pitch.length", Integer.toString(size));
        context.put("pitch.angle.min", Double.toString(-geneticParams.getPitchDeltaDev()));
        context.put("pitch.angle.max", Double.toString(geneticParams.getPitchDeltaDev()));

        PoolFactory poolFactory = new DefaultPoolFactory();
        return poolFactory.createPool(populationCount, FullSolution.PitchSolution.class, context);
    }

    @Override
    public void applySolution(FullSolution.PitchSolution solution, ModellingInstance modellingInstance) {
        System.out.println("OnlyPitchGeneticLauncher.applySolution");
        PieceLinearFunction pitchControlProgram = modellingInstance.getModellingManager().getLaunch().getPichControlProgram();
        Iterator<Double> iterator = pitchControlProgram.getFunctionArguments().iterator();
        int i = 0;
        while (iterator.hasNext()) {
            Double t = iterator.next();
            if (t == 0.1d) continue;
            double initialValue = initialPitchFunction.getFunctionValue(t);
            double deltaValue = Math.toRadians(solution.getPitchValues().get(i++).getDPitch());
            double value = initialValue + deltaValue;
            System.out.println(String.format("f(%s)=%s, d=%s", t, value, deltaValue));
            pitchControlProgram.setFunctionValue(value, t);
            System.out.println(String.format("f(%s)=%s", t, value));
        }
    }
}