package ru.etu.sim.launchers.genetics;

import net.sourceforge.evoj.GenePool;
import net.sourceforge.evoj.core.DefaultPoolFactory;
import ru.etu.sim.genetic.FullSolution;
import ru.etu.sim.genetic.GeneticParams;
import ru.etu.sim.launchers.GeneticLauncher;
import ru.etu.sim.launchers.LaunchParams;
import ru.etu.sim.launchers.ModellingInstance;
import ru.spb.nicetu.launchsimulation.simcore.math.specificmatrixalgebra.GeographicCoordinatePoint;
import ru.spb.nicetu.launchsimulation.simtestdata.TestDataInits;

import java.util.HashMap;
import java.util.Map;

/**
 * @author bazleks
 */
public class OnlyTargetGeneticLauncher extends GeneticLauncher<FullSolution.TargetSolution> {

    private GeographicCoordinatePoint defaultPoint;

    public OnlyTargetGeneticLauncher(GeneticParams geneticParams,
                                     GeneticLauncherListener<FullSolution.TargetSolution> listener) {
        super(geneticParams, listener);
    }

    @Override
    public void setLaunchParams(LaunchParams launchParams) {
        super.setLaunchParams(launchParams);
        if (launchParams.getAimingPoint() == null) {
            this.defaultPoint = new GeographicCoordinatePoint();
            TestDataInits.variant = launchParams.getDataInitsVariant();
            this.defaultPoint = new GeographicCoordinatePoint(TestDataInits.getTargetPoint());
        } else {
            this.defaultPoint = new GeographicCoordinatePoint(launchParams.getAimingPoint());
        }
    }

    @Override
    public GenePool<FullSolution.TargetSolution> createGenePool(int populationCount) {
        Map<String, String> context = new HashMap<>();
        context.put("latitude.min", Double.toString(-geneticParams.getTargetDeltaDev()));
        context.put("latitude.max", Double.toString(geneticParams.getTargetDeltaDev()));
        context.put("longitude.min", Double.toString(-geneticParams.getTargetDeltaDev()));
        context.put("longitude.max", Double.toString(geneticParams.getTargetDeltaDev()));
        return new DefaultPoolFactory().createPool(populationCount, FullSolution.TargetSolution.class, context);
    }

    @Override
    public void applySolution(FullSolution.TargetSolution solution, ModellingInstance modellingInstance) {
        GeographicCoordinatePoint targetPoint = modellingInstance.getModellingManager().getLaunch().getTargetPoint();
        targetPoint.setLatitude(defaultPoint.getLatitude() + Math.toRadians(solution.getDLat()));
        targetPoint.setLongitude(defaultPoint.getLongitude() + Math.toRadians(solution.getDLong()));
    }
}
