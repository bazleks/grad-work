package ru.etu.sim.launchers;

import ch.qos.logback.classic.Level;
import ru.etu.sim.launchers.LaunchParams;
import ru.etu.sim.utils.DistanceUtils;
import ru.spb.nicetu.launchsimulation.simcore.math.specificmatrixalgebra.GeographicCoordinatePoint;
import ru.spb.nicetu.launchsimulation.simcore.result.ParameterDictionary;
import ru.spb.nicetu.launchsimulation.simcore.result.ResultAccumulator;
import ru.spb.nicetu.launchsimulation.simcore.result.ResultAccumulatorController;
import ru.spb.nicetu.launchsimulation.simcore.simobject.Launch;
import ru.spb.nicetu.launchsimulation.simcore.simproc.ModellingManager;
import ru.spb.nicetu.launchsimulation.simtestdata.TestDataConstructor;
import ru.spb.nicetu.launchsimulation.simtestdata.TestDataInits;

import java.util.List;

/**
 * Created by rpgso_000 on 07.06.2015.
 */
public class ModellingInstance {
    protected double missingDistance;
    protected LaunchListener launchListener;
    protected ModellingManager modellingManager;
    protected ResultAccumulator resultAccumulator;
    protected GeographicCoordinatePoint landingPoint;

    public ModellingInstance(LaunchParams launchParams) {
        resultAccumulator = ResultAccumulatorController.getInstance().newResultAccumulator(1);
        resultAccumulator.setPointSavingRate(15);
        this.modellingManager = new ModellingManager(Level.OFF);
        this.modellingManager.init(createLaunch(launchParams));
    }

    public double getMissingDistance() {
        return missingDistance;
    }

    public ModellingManager getModellingManager() {
        return modellingManager;
    }

    public GeographicCoordinatePoint getLandingPoint() {
        return landingPoint;
    }

    public void setLaunchListener(LaunchListener launchListener) {
        this.launchListener = launchListener;
    }

    @SuppressWarnings("unchecked")
    public void calculate() {
        modellingManager.run();
        GeographicCoordinatePoint targetPoint = modellingManager.getLaunch().getTargetPoint();
        List<Double> landingPointList = resultAccumulator.getResultList().getValues(ParameterDictionary.LANDING_POINT);
        landingPoint = new GeographicCoordinatePoint(landingPointList.get(0), landingPointList.get(1), 0);
        missingDistance = DistanceUtils.computeDistanceFromDecimal(
                targetPoint.getLatitude(), targetPoint.getLongitude(),
                landingPoint.getLatitude(), landingPoint.getLongitude()
        );
    }

    private static Launch createLaunch(LaunchParams params) {
        Launch launch;
        if (params.getDataInitsVariant() == null) {
            launch = new Launch();
            launch.setStartPoint(params.getStartedPoint());
            launch.setTargetPoint(params.getAimingPoint());
        } else {
            TestDataInits.variant = params.getDataInitsVariant();
            launch = TestDataConstructor.createTestLaunch(1L, params.getDataInitsVariant(), TestDataInits.SubVariants.NORMAL);
        }
        launch.setPichControlProgram(params.getPitchProgram());
        launch.setCalculationStrategy(params.getCalculationStrategy());
        return launch;
    }

    public static interface LaunchListener {
        void onFinish();
    }
}
