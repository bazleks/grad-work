package ru.etu.sim.launchers;

import net.sourceforge.evoj.*;
import net.sourceforge.evoj.handlers.DefaultHandler;
import net.sourceforge.evoj.strategies.sorting.SolutionRating;
import ru.etu.sim.genetic.GeneticParams;

/**
 * @author bazleks
 */
public abstract class GeneticLauncher<T> extends AbstractLauncher {
    public interface GeneticLauncherListener<T> extends LauncherListener {

        void onNextSolutionStart(ModellingInstance modellingInstance);

        void onNextSolutionDone(ModellingInstance modellingInstance, T solution);
    }

    protected GeneticParams geneticParams;
    protected MutationStrategy mutationStrategy;
    protected SelectionStrategy selectionStrategy;
    protected ReplicationStrategy replicationStrategy;

    private T bestSolution;

    public GeneticLauncher(GeneticParams geneticParams, GeneticLauncherListener<T> listener) {
        super(listener);
        this.geneticParams = geneticParams;
    }

    @Override
    public GeneticLauncherListener<T> getLauncherListener() {
        return (GeneticLauncherListener<T>) super.getLauncherListener();
    }

    @Override
    public void launch() {
        final GeneticLauncherListener<T> launcherListener = getLauncherListener();
        GenePool<T> genePool = createGenePool(geneticParams.getPopulationCount());
        SolutionRating<T, Double> solutionRating = new SolutionRating<T, Double>() {
            @Override
            public Double calcRating(T solution) {
                try {
                    ModellingInstance modellingInstance = new ModellingInstance(launchParams);
                    applySolution(solution, modellingInstance);
                    if (launcherListener != null) {
                        launcherListener.onNextSolutionStart(modellingInstance);
                    }
                    modellingInstance.calculate();
                    if (launcherListener != null) {
                        launcherListener.onNextSolutionDone(modellingInstance, solution);
                    }
                    System.out.println("Missing distance: " + modellingInstance.getMissingDistance());
                    return -modellingInstance.getMissingDistance();
                } catch (Exception e) {
                    System.out.println("PROBLEM, RETURN NULL");
                    e.printStackTrace();
                    return null;
                }
            }
        };
        PoolHandler<T> poolHandler = new DefaultHandler<>(
                solutionRating,
                mutationStrategy,
                replicationStrategy,
                selectionStrategy
        );
        poolHandler.iterate(genePool, geneticParams.getIterationsCount());
        ModellingInstance modellingInstance = new ModellingInstance(launchParams);
        bestSolution = genePool.getBestSolution();
        applySolution(genePool.getBestSolution(), modellingInstance);
        modellingInstance.calculate();
        if (launcherListener != null) launcherListener.onFinish(modellingInstance);
    }

    public abstract GenePool<T> createGenePool(int populationCount);

    public abstract void applySolution(T solution, ModellingInstance modellingInstance);

    public T getBestSolution() {
        return bestSolution;
    }
}
