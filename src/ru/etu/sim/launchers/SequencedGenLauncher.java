package ru.etu.sim.launchers;

import ru.etu.sim.genetic.GeneticParams;

/**
 * @author bazleks
 */
public abstract class SequencedGenLauncher<T1 extends GeneticLauncher, T2 extends GeneticLauncher> extends AbstractLauncher {

    protected T1 firstLauncher;
    protected T2 secondLauncher;

    public SequencedGenLauncher(LauncherListener launcherListener) {
        super(launcherListener);
    }
}
