package ru.etu.sim.launchers;

/**
 * Created by rpgso_000 on 07.06.2015.
 */
public abstract class AbstractLauncher {

    public interface LauncherListener {
        void onFinish(ModellingInstance instance);
    }

    private LauncherListener launcherListener;
    protected LaunchParams launchParams;

    public AbstractLauncher(LauncherListener launcherListener) {
        this.launcherListener = launcherListener;
    }

    public void setLaunchParams(LaunchParams launchParams) {
        this.launchParams = launchParams;
    }

    public void setLauncherListener(LauncherListener launcherListener) {
        this.launcherListener = launcherListener;
    }

    public abstract void launch();

    public LauncherListener getLauncherListener() {
        return launcherListener;
    }
}
