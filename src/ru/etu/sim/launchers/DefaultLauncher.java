package ru.etu.sim.launchers;

/**
 * Created by rpgso_000 on 07.06.2015.
 */
public class DefaultLauncher extends AbstractLauncher {

    public DefaultLauncher(LauncherListener launcherListener) {
        super(launcherListener);
    }

    @Override
    public void launch() {
        ModellingInstance modellingInstance = new ModellingInstance(launchParams);
        modellingInstance.calculate();
        getLauncherListener().onFinish(modellingInstance);
    }
}
