package ru.etu.sim.launchers;

import ru.spb.nicetu.launchsimulation.simcore.math.function.PieceLinearFunction;
import ru.spb.nicetu.launchsimulation.simcore.math.specificmatrixalgebra.GeographicCoordinatePoint;
import ru.spb.nicetu.launchsimulation.simcore.simobject.rocket.state.CalculationStrategy;
import ru.spb.nicetu.launchsimulation.simtestdata.TestDataInits;

/**
 * @author bazleks
 */
public class LaunchParams {

    // evolving parameters
    private PieceLinearFunction pitchProgram;
    private GeographicCoordinatePoint aimingPoint;

    // constant parameters
    private CalculationStrategy calculationStrategy;
    private TestDataInits.Variants dataInitsVariant;
    private GeographicCoordinatePoint startedPoint;

    public static final PieceLinearFunction defaultKapustinYarPitch = new PieceLinearFunction();

    static {
        defaultKapustinYarPitch.setFunctionValue(90.0d, 0.1d);
        defaultKapustinYarPitch.setFunctionValue(85.0d, 10.0d);
        defaultKapustinYarPitch.setFunctionValue(65.0d, 125.0d);
        defaultKapustinYarPitch.setFunctionValue(45.0d, 130.0d);
        defaultKapustinYarPitch.setFunctionValue(-14.781d, 135.0d);
        defaultKapustinYarPitch.setFunctionValue(-18.43d, 186.0d);
    }

//    public static Map<Double, Double> defaultPitchKapustinYar = new TreeMap<Double, Double>() {{
//        put(0.1d, 90.0d);
//        put(10.0d, 85.0d);
//        put(125.0d, 65.0d);
//        put(130.0d, 45.0d);
//        put(135.0d, -14.781d);
//        put(186.0d, -18.43d);
//    }};


    public PieceLinearFunction getPitchProgram() {
        return pitchProgram;
    }

    public void setPitchProgram(PieceLinearFunction pitchProgram) {
        this.pitchProgram = pitchProgram;
    }

    public GeographicCoordinatePoint getAimingPoint() {
        return aimingPoint;
    }

    public void setAimingPoint(GeographicCoordinatePoint aimingPoint) {
        this.aimingPoint = aimingPoint;
    }

    public CalculationStrategy getCalculationStrategy() {
        return calculationStrategy;
    }

    public void setCalculationStrategy(CalculationStrategy calculationStrategy) {
        this.calculationStrategy = calculationStrategy;
    }

    public TestDataInits.Variants getDataInitsVariant() {
        return dataInitsVariant;
    }

    public void setDataInitsVariant(TestDataInits.Variants dataInitsVariant) {
        this.dataInitsVariant = dataInitsVariant;
    }

    public GeographicCoordinatePoint getStartedPoint() {
        return startedPoint;
    }

    public void setStartedPoint(GeographicCoordinatePoint startedPoint) {
        this.startedPoint = startedPoint;
    }
}
