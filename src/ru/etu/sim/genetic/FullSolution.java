package ru.etu.sim.genetic;

import net.sourceforge.evoj.core.annotation.ListParams;
import net.sourceforge.evoj.core.annotation.Range;

import java.util.List;

/**
 * Created by rpgso_000 on 04.06.2015.
 */
public interface FullSolution {
    TargetSolution getTargetSolution();

    PitchSolution getPitchSolution();

    interface TargetSolution {
        @Range(min = "latitude.min", max = "latitude.max")
        double getDLat();

        @Range(min = "longitude.min", max = "longitude.max")
        double getDLong();
    }

    interface PitchSolution {
        @ListParams(length = "pitch.length")
        List<PitchDelta> getPitchValues();

        interface PitchDelta {
            @Range(min = "pitch.angle.min", max = "pitch.angle.max")
            double getDPitch();
        }
    }
}
