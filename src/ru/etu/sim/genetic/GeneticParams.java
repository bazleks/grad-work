package ru.etu.sim.genetic;

/**
 * Created by rpgso_000 on 02.06.2015.
 */
public class GeneticParams {
    private int populationCount;
    private int iterationsCount;
    private double targetDeltaDev, pitchDeltaDev;
    private double targetMutAff, pitchMutAff;

    private GAStrategy strategy = GAStrategy.ONLY_TARGET;

    public int getPopulationCount() {
        return populationCount;
    }

    public GAStrategy getStrategy() {
        return strategy;
    }

    public void setPopulationCount(int populationCount) {
        this.populationCount = populationCount;
    }

    public void setIterationsCount(int iterationsCount) {
        this.iterationsCount = iterationsCount;
    }

    public void setStrategy(GAStrategy strategy) {
        this.strategy = strategy;
    }

    public int getIterationsCount() {
        return iterationsCount;
    }

    public double getTargetDeltaDev() {
        return targetDeltaDev;
    }

    public double getPitchDeltaDev() {
        return pitchDeltaDev;
    }

    public double getTargetMutAff() {
        return targetMutAff;
    }

    public double getPitchMutAff() {
        return pitchMutAff;
    }

    public void setTargetDeltaDev(double targetDeltaDev) {
        this.targetDeltaDev = targetDeltaDev;
    }

    public void setPitchDeltaDev(double pitchDeltaDev) {
        this.pitchDeltaDev = pitchDeltaDev;
    }

    public void setTargetMutAff(double targetMutAff) {
        this.targetMutAff = targetMutAff;
    }

    public void setPitchMutAff(double pitchMutAff) {
        this.pitchMutAff = pitchMutAff;
    }

    public enum GAStrategy {
        ONLY_TARGET, ONLY_PITCH, MIXED, SEQUENCED
    }

    @Override
    public String toString() {
        return "GeneticParams{" +
                "populationCount=" + populationCount +
                ", iterationsCount=" + iterationsCount +
                ", targetDeltaDev=" + targetDeltaDev +
                ", pitchDeltaDev=" + pitchDeltaDev +
                ", targetMutAff=" + targetMutAff +
                ", pitchMutAff=" + pitchMutAff +
                ", strategy=" + strategy +
                '}';
    }
}
