package ru.etu.sim.utils;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by rpgso_000 on 04.06.2015.
 */
public class DistanceUtils {
    public static Double computeDistance(List<Double> latitude, List<Double> longitude, List<Double> latitudeLand, List<Double> longitudeLand) {
        double degree = DegreesUtils.getDegree(latitude.get(2), latitude.get(1), latitude.get(0));
        double degreeLand = DegreesUtils.getDegree(latitudeLand.get(2), latitudeLand.get(1), latitudeLand.get(0));
        //this formula comes from rop393
        double r = 6378140 * (0.99832407 + 0.00167644
                * round(Math.cos(2 * degree), 12)
                - 0.00000352 * round(Math.cos(4 * degree), 12));
        return round(r * Math.acos(round(Math.sin(degree), 12) * round(Math.sin(degreeLand), 12)
                + round(Math.cos(degree), 12) * round(Math.cos(degreeLand), 12)
                * round(Math.cos(DegreesUtils.getDegree(longitude.get(2), longitude.get(1), longitude.get(0))
                - DegreesUtils.getDegree(longitudeLand.get(2), longitudeLand.get(1), longitudeLand.get(0))), 12)), 2);
    }

    private static double round(double number, int scale) {
        return new BigDecimal(String.valueOf(number)).setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static Double computeDistanceFromDecimal(double lg, double lt, double landLg, double landLt) {
        List<Double> latitude = DegreesUtils.getDegree(lg);
        List<Double> longitude = DegreesUtils.getDegree(lt);
        List<Double> latitudeLand = DegreesUtils.getDegree(landLg);
        List<Double> longitudeLand = DegreesUtils.getDegree(landLt);
        return computeDistance(latitude, longitude, latitudeLand, longitudeLand);
    }
}
