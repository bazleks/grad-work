/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.etu.sim.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Sergey
 */
public class DegreesUtils {
    /**
     * Gets the degree, minutes and seconds from the float value of degree.
     *
     * @param numb the numb
     * @return the degree
     */
    public static List<Double> getDegree(double numb) {
        double gradRad = (numb * 180) / Math.PI;
        double grad = (int) gradRad;
        double secAll = (gradRad - (int) gradRad) * 3600;
        double min = (int) (secAll / 60.0);
        double sec = (secAll / 60.0 - min) * 60;
        if (Double.compare(roundDouble(sec), 60.0d) == 0) {
            sec = 0.0;
            min = min + 1;
        }
        if (Double.compare(roundDouble(min), 60.0d) == 0) {
            min = 0.0;
            grad = grad + 1;
        }
        return Arrays.asList(grad, min, sec);
    }

    public static double getDegreeFromDecimal(Double degrees) {
        ArrayList<Double> list = new ArrayList<>(getDegree((degrees * Math.PI) / 180d));
        final double dD = (list.get(2) / 3600) + (list.get(1) / 60) + list.get(0);
        return (dD * Math.PI) / 180d;
    }

    /**
     * Gets the degree in decimal format from the radians value of degree.
     *
     * @param numb the numb
     * @return the degree
     */
    public static Double getDegreeDecimal(double numb) {
        double gradRad = (numb * 180) / Math.PI;
        double grad = (int) gradRad;
        double secAll = (gradRad - (int) gradRad) * 3600;
        double min = (int) (secAll / 60.0);
        double sec = (secAll / 60.0 - min) * 60;
        return grad + (min + sec / 60.0) / 60.0;
    }

    public static double getDegree(final Double seconds, final Double minutes, final Double degrees) {
        final double dD = (seconds / 3600) + (minutes / 60) + degrees;
        return (dD * Math.PI) / 180d;
    }

    public static double roundDouble(double value) {
        return Math.round(value * 1000.0) / 1000.0;
    }
}
